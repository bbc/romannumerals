﻿// *****************************************************************
// RomanNumerals
// RomanNumericalTests
// Tests.cs
// Created : 09/07/2014
// Modified : 03/12/2014
// Daniel M. Clegg

using System.Collections.Generic;
using Numbers;
using NUnit.Framework;

namespace RomanNumericalTests
{
    [TestFixture]
    public class Tests
    {
        private Dictionary<string, int> _numerals;

        [SetUp]
        public void Setup()
        {
            _numerals = new Dictionary<string, int>
            {
                {"I", 1},
                {"II", 2},
                {"III", 3},
                {"IV", 4},
                {"V", 5},
                {"VI", 6},
                {"VII", 7},
                {"VIII", 8},
                {"IX", 9},
                {"X", 10},
                {"XI", 11},
                {"XII", 12},
                {"XIII", 13},
                {"XIV", 14},
                {"XV", 15},
                {"XVI", 16},
                {"XVII", 17},
                {"XVIII", 18},
                {"XIX", 19},
                {"XX", 20},
                {"XXX", 30},
                {"XL", 40},
                {"L", 50},
                {"LX", 60},
                {"LXX", 70},
                {"LXXX", 80},
                {"XC", 90},
                {"C", 100},
                {"D", 500},
                {"M", 1000},
                {"MM", 2000},
                {"MMMCMXCIX", 3999}
            };
        }

        [Test]
        public void TestNumerals()
        {
            foreach (var numeral in _numerals)
            {
                var num = new RomanNumerals();

                Assert.IsTrue(num.Generate(numeral.Value) == numeral.Key, "Test Failed on " + numeral.Key + " " + numeral.Value + " " + num.Generate(numeral.Value));
            }
        }
    }
}