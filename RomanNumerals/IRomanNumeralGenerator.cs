// *****************************************************************
// RomanNumerals
// RomanNumerals
// IRomanNumeralGenerator.cs
// Created : 03/12/2014
// Modified : 03/12/2014
// Daniel M. Clegg

using System;

namespace Numbers
{
    public interface IRomanNumeralGenerator
    {
        String Generate(int number);
    }
}