﻿using System;
using System.Linq;

namespace Numbers
{
    public static class Extensions
    {
        #region Public Method[s]

        public static string Repeat(this string s, int n)
        {
            return new String(Enumerable.Range(0, n).SelectMany(x => s).ToArray());
        }

        #endregion
    }
}