﻿// *****************************************************************
// RomanNumerals
// RomanNumerals
// RomanNumeral.cs
// Created : 03/12/2014
// Modified : 03/12/2014
// Daniel M. Clegg

namespace Numbers
{
    public class RomanNumeral
    {
        public int DecimalValue { get; set; }
        public string RomanNumeralValue { get; set; }
    }
}