﻿// *****************************************************************
// RomanNumerals
// RomanNumerals
// RomanNumerals.cs
// Created : 09/07/2014
// Modified : 03/12/2014
// Daniel M. Clegg

using System.Collections.Generic;
using System.Linq;

namespace Numbers
{
    public class RomanNumerals : IRomanNumeralGenerator
    {
        private List<RomanNumeral> _numerals = new List<RomanNumeral>();

        public RomanNumerals()
        {
            Initialise();
        }

        public string Generate(int value)
        {
            var working = value;
            var answer = string.Empty;

            foreach (var numeral in _numerals)
            {
                if (working >= numeral.DecimalValue)
                {
                    var count = working/numeral.DecimalValue;

                    answer += numeral.RomanNumeralValue.Repeat(count);

                    working -= (numeral.DecimalValue*count);

                    if (working == 0)
                        break;
                }
            }

            return answer;
        }

        private void Add(
            string romanNumeralValue,
            int decimalValue)
        {
            _numerals.Add(new RomanNumeral
            {
                RomanNumeralValue = romanNumeralValue,
                DecimalValue = decimalValue
            });
        }

        private void Initialise()
        {
            Add("M", 1000);
            Add("CM", 900);
            Add("D", 500);
            Add("CD", 400);
            Add("C", 100);
            Add("XC", 90);
            Add("L", 50);
            Add("XL", 40);
            Add("X", 10);
            Add("IX", 9);
            Add("V", 5);
            Add("IV", 4);
            Add("I", 1);

            _numerals = _numerals.OrderByDescending(c => c.DecimalValue).ToList();
        }
    }
}